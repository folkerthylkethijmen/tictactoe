TicTacToe  Version 1.0  05-06-2018
==============

TicTacToe is a web-based 2-player tic tac toe game, created with Javascript and PHP.
Users can play from seperate devices and can communicate through a chat feature.

Getting Started
--------------

To play TicTacToe [online](http://siegfried.webhosting.rug.nl/~s3360695/tictactoe/) no download is required.
The source code can be downloaded from this repository.

###Installing

For local use, clone the directory or download the following:

- All PHP files from the main folder
- 'css' folder (contains all CSS style files)
- 'js' folder (contains all Javascript files)
- 'db' folder (contains all JSON data files)

Deployment
--------------

TicTacToe cannot be played locally because it requires 2 players to take part.

Authors
--------------

- **Thijmen Dam** - [s3360695](mailto:t.m.dam@student.rug.nl)
- **Folkert Leistra** - [s3469573](mailto:f.a.leistra@student.rug.nl)
- **Hylke van der Veen** - [s3348431](mailto:h.f.van.der.veen@student.rug.nl)

Other
--------------

Documentation on all files can be found in [files.md](files.md).
