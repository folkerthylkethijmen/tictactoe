<?PHP
// Onthoudt de clicks van het huidige spel in game.json
// Houdt ook de 'aan zet' label correct
if (isset($_POST['id']) && isset($_POST['html'])) {
    $data = file_get_contents("db/game.json");
    $turn_data = file_get_contents('db/turn.json');
    $ids = json_decode($data);
    $turn = json_decode($turn_data);
    $id = $_POST["id"];
    $html = $_POST["html"];
    $ids[] = array($id => $html);

    if (in_array(0, $turn)) {
        $turn[0] = array(1);
        $turn_output = json_encode($turn[0]);
        file_put_contents("db/turn.json", $turn_output);
    } else {
        $turn[0] = array(0);
        $turn_output = json_encode($turn[0]);
        file_put_contents("db/turn.json", $turn_output);
    }

    $output = json_encode($ids);
    file_put_contents("db/game.json", $output);

}
?>
