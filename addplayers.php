<?PHP
if (isset($_POST['username'])) {
    $data = file_get_contents("db/players.json");
    $users = json_decode($data);
    $username = htmlspecialchars($_POST['username']);
    $users[] = array("username" => $username);
    $output = json_encode($users);
    file_put_contents("db/players.json", $output);
}
?>