TicTacToe Files Documentation
===============

This file gives a complete overview of all PHP, Javascript, CSS and JSON files used for TicTacToe.

#PHP

All PHP files can be found in the main folder.

##index.php

Index.php is the homepage of TicTacToe. It consists of an interface where the user can enter their name and join a game.
If another user is already searching for a game, they will be directed to `tictactoe.php`.
If no other user is searching, the page waits until someone else is searching too.

##tictactoe.php

On tictactoe.php the game is played. It consists of four main elements:

- a bar in the top left displaying the current users,
- a bar in the top right displaying which users' turn it is and the final score,
- the tic tac toe game field in the bottom left,
- a chat box in the bottom right for the players to communicate through.

Every time a game is finished, the field resets, but the chat history stays.

##addplayers.php

addplayers.php is used to add the user input from `index.php` to `players.json`. 

##addclicks.php

addclicks.php is used to match the clicked field to the proper user and push the array to `game.json`.

##chat.php

chat.php retrieves chat.json data and pushes the new messages.

##win.php

win.php executes when a game is finished (either by win or tie). It empties `game.json` and sets `turn.json` to 0.

##reset.php

reset.php resets `game.json`, `turn.json` and `chat.json` once 2 players have submitted their name on index.php so there is no game data upon game start.

##exit.php

exit.php resets all data including chat once a player loads the index page.

#Javascript

All Javascript files can be found in the 'js' folder.

##index.js

index.js takes care of all of the javascript surrounding `index.php`.

- `function checkPlayers()` sends user to `tictactoe.php` if `players.json` contains 2 players.
- `$('#submit').on('click', function (e)` saves entered name to `players.json`.
- `function setCookie()` sets a user cookie, which will be used later on.
- `function makeReadOnly()` makes sure user cannot enter another name in the entry box.
- `function checkWaiting()` sets waiting indicator for players who do not have an opponent yet.

##tictactoe.js

tictactoe.js deals with all the Javascript for `tictactoe.php`

- `let markers` is an array containing the 'X' and 'O' marker for later use.
- `function fetchPlayers()` retrieves the players from `players.json` and assigns them to variables.
- `function getDatabase()` retrieves the current game progress from `game.json`.
- `function aanZet()` changes the 'aan zet' label according to the turn variable. The turn variable is retrieved from `turn.json`.
- `function victory()` is one big ass clusterfuck i dont know whats going on inside there
- `$(".ttt_btn").on("click", function()` saves the clicked button with user in `game.json`. Calls functions `getDatabase()`, `aanZet()` and `victory()` to refresh the field.

##chat.js
- `function getCookie()` reads the player's name from a cookie file
- `$('#send_btn').on('click', function (e)` handles the click functionality of the 'send message' button.
- `function loadChat()` loads the chat when after finishing a game and people keep playing.



#CSS

All CSS files can be found in the 'css' folder.

##index.css

Provides the CSS code for the HTML on the `index.php` page. 

##tictactoe.css

Provides the CSS code for the HTML on the `tictactoe.php` page.

#JSON

All JSON files can be found in the 'db' folder.

##players.json

players.json is an array with the users' names. It has a maximum length of two, as this is the maximum number of players.

##game.json

game.json stores all moves in a game. It consists of one array with objects for each clicked button (square in the game).
Each object consists of the button ID followed by the HTML code for either the 'X' or the 'O'.

##turn.json

turn.json is an array with a single binary digit. It is used to keep track of whose turn it is throughout the game.
The digit is also used to indicate which name and marker ('X' or 'O') should be used.

##chat.json

chat.json contains the chat history from the two current players. When new users enter their name, it empties.
