<?PHP
if (isset($_GET['ajax'])) {
    $data = file_get_contents("db/players.json"); // Laadt JSON
    header('Content-Type: application/json'); // Stelt header in
    echo $data;
    die();
}
?>

<!doctype html>
<!-- Dit bestand betreft de indexpagina van TicTacToe -->
<html lang="nl">

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/index.css">
    <link href="https://fonts.googleapis.com/css?family=Bowlby+One+SC" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Tic Tac Toe</title>
</head>

<body>

<div class="main_container">

    <h1>Tic Tac Toe</h1>
    <form>
        <label for="name">Gebruikersnaam</label>
        <input type="text" name="name" id="name" required>
        <input id="submit" type="submit" value="Spelen">
    </form>

    <span id="waiting">
        <i class="fa fa-spinner fa-spin"></i>
        Wachten op spelers...
    </span>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/index.js"></script>
</body>
</html>