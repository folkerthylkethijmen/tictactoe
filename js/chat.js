$(document).ready(function ($) {

    // Leest de naam-cookie
    function getCookie(name) {
      let value = "; " + document.cookie;
      let parts = value.split("; " + name + "=");
      if (parts.length === 2) return parts.pop().split(";").shift();
    }

    // Handelt de clicks van de verzendbutton af
    $('#send_btn').on('click', function (e) {
        e.preventDefault();
        let msg = document.getElementById("msg_send").value;
        // Haalt de player's naam binnen en koppelt deze aan de juiste class
        let p1_name = document.getElementById("p1").innerHTML;
        let myname = getCookie("naam");
        let myclass;
        let otherclass;
        if (myname === p1_name) {
            myclass = "msg1";
        } else {
            myclass = "msg2";
        }

        // Voegt HTML toe aan de chat
        let msg_html = `<p class="msg ${myclass}">${myname}: ${msg}</p>`;

        $.ajax({
            method: "POST",
            url: "chat.php",
            data: {html: msg_html, class: myclass}
        }).done(function () {
            document.getElementById("msg_send").value = ""
        });

    });

    // Houdt chatbox up-to-date
    let previous = null;
    let current = null;
    setInterval(function() {
        let chatbox = document.getElementById("chatbox");
        $.getJSON("db/chat.json", function(json) {
            current = JSON.stringify(json);
            if (previous && current && previous !== current) {
                chatbox.innerHTML += json.slice(-1)[0]["html"]
            }
            previous = current;
        });
    }, 500);

    // Laadt alle chat in het begin van een game
    function loadChat() {
        let chatbox = document.getElementById("chatbox");
        $.getJSON("db/chat.json", function(json) {
            for (let msg in json) {
                console.log(json[msg]["html"]);
                chatbox.innerHTML += json[msg]["html"]
            }
        })
    }

    loadChat();

});