$(document).ready(function ($) {

    // Leegt de playerdatabase
    let check = false;
    $.ajax({method: "POST", url: "exit.php"}).done(function(){
        check = true;
    });

    function checkPlayers() {
        //Controleert of er 2 players aanwezig zijn in players.json -> zo ja dan gaat de window naar het spel
        // Tic Tac Toe
        if (check === true) {
            $.getJSON("db/players.json", function (json) {
                if (json.length === 2) {
                    $.ajax({method: "POST", url: "reset.php"});
                    window.location.replace("tictactoe.php");
                }
            });
        }
    }

    // Zorgt ervoor dat wanneer je op de Submit knop drukt de username data wordt opgeslagen in players.json
    $('#submit').on('click', function (e) {
        if ($('#name').val().length !== 0) {
            e.preventDefault();
            /* Voer een POST request uit op add.php, met als velden title en content en de datum*/
            $.ajax({method: "POST", url: "addplayers.php", data: {username: $('#name').val()}})
            .done(function (data) {
                setCookie($('#name').val());
                makeReadOnly();
            });
        }
    });

    // Zet cookie om players te onthouden
    function setCookie(name) {
        document.cookie = `naam=${name}; path=/`;
    }

    // Maakt button + textbox readonly na click
    function makeReadOnly() {
        let name_input = document.getElementById("name");
        name_input.readOnly = true;
        let play_btn = document.getElementById("submit");
        play_btn.style.pointerEvents = "none";
        play_btn.style.backgroundColor = "#e1e2e5";
        checkWaiting();
    }

    // Kijkt of de players.json database = 0
    // Zo ja, show waiting indicator
    function checkWaiting() {
        $.getJSON("db/players.json", function (json) {
            if (json.length === 0) {
                document.getElementById("waiting").style.display = "block";
            }
        });
    }

    setInterval(checkPlayers, 500);

});