$(document).ready(function ($) {

    console.log(document.cookie);

    let markers = ['\'fas fa-times\'', '\'far fa-circle\''];
    // Namen aanwezige spelers bovenaan het spel plaatsen
    function fetchPlayers() {
        $.ajax("tictactoe.php?ajax=true")
            .done(function (data) {
                let player1 = data[0]["username"];
                let player2 = data[1]["username"];


                $(".name1").append(`<span id="p1">${player1}</span>`);

                $(".name2").append(`<span id="p2">${player2}</span>`);

            })
    }

    fetchPlayers();


    function getDatabase() {
        // laadt de data uit game.json en voegt vervolgens in het speelvled op de gewenste locatie een X of O toe.
        $.getJSON("db/game.json", function(json) {
                for (let i in json) {
                    let obj = json[i];
                    for (let x in obj) {
                        let id = x;
                        let val = obj[x];
                        document.getElementById(id).innerHTML = val;
                    }}
        });
    }
    function aanZet() {
        // Verandert de player label rechtsbovenin, aan de hand van de turn variable (turn.json).
        $.getJSON('db/turn.json', function (json) {
            let turn = json[0];
            $.getJSON("db/players.json", function (json) {
                let name_lbl = document.getElementById("name");
                name_lbl.innerHTML = "Aan zet: <br>" + json[turn]["username"];
            });
        });
    }

    function victory() {
        // Controleert na elke zet of er iemand heeft gewonnen aan de hand van  de inhoud van 2 arrays: xboxes en oboxes
        let xboxes = [];
        let oboxes = [];
        let name_lbl = document.getElementById("name");
        $.getJSON("db/game.json", function ( json ) {
            for (let i in json) {
                let obj = json[i];
                for (let x in obj) {
                    if (obj[x] === "<i class='fas fa-times'></i>" ) {
                        xboxes.push(x);
                        $.getJSON("db/players.json", function (players) {
                            let player1 = players[0]["username"];
                            if (xboxes.includes("one") && xboxes.includes("two") && xboxes.includes("three")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player1 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            if (xboxes.includes("four") && xboxes.includes("five") && xboxes.includes("six")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player1 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            if (xboxes.includes("seven") && xboxes.includes("eight") && xboxes.includes("nine")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player1 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            if (xboxes.includes("one") && xboxes.includes("four") && xboxes.includes("seven")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player1 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            if (xboxes.includes("two") && xboxes.includes("five") && xboxes.includes("eight")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player1 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            if (xboxes.includes("three") && xboxes.includes("six") && xboxes.includes("nine")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player1 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            if (xboxes.includes("one") && xboxes.includes("five") && xboxes.includes("nine")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player1 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            if (xboxes.includes("three") && xboxes.includes("five") && xboxes.includes("seven")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player1 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}})}
                    if (obj[x] === "<i class='far fa-circle'></i>") {
                        oboxes.push(x);
                        $.getJSON("db/players.json", function (data) {
                            let player2 = data[1]["username"];
                            if (oboxes.includes("one") && oboxes.includes("two") && oboxes.includes("three")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player2 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            if (oboxes.includes("four") && oboxes.includes("five") && oboxes.includes("six")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player2 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            if (oboxes.includes("seven") && oboxes.includes("eight") && oboxes.includes("nine")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player2 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            if (oboxes.includes("one") && oboxes.includes("four") && oboxes.includes("seven")) {
                                clearInterval(penint);
                                clearInterval(vicint);
                                name_lbl.innerHTML = "";
                                name_lbl.innerHTML =  player2 + " wint!";
                                $.ajax({method: "POST", url: "win.php"
                                })
                                    .done(function () {
                                        newGame();
                                    })
                            }
                            if (oboxes.includes("two") && oboxes.includes("five") && oboxes.includes("eight")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player2 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            if (oboxes.includes("three") && oboxes.includes("six") && oboxes.includes("nine")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player2 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            if (oboxes.includes("one") && oboxes.includes("five") && oboxes.includes("nine")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player2 + " wint!";
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            if (oboxes.includes("three") && oboxes.includes("five") && oboxes.includes("seven")) {
                                clearInterval(penint);
                                setTimeout(function () {
                                    name_lbl.innerHTML = "";
                                    name_lbl.innerHTML = player2 + " wint!";
                                    console.log(name_lbl);
                                    $.ajax({
                                        method: "POST", url: "win.php"
                                    })
                                        .done(function () {
                                            setTimeout(function () {
                                                location.reload(true); // forces a reload from the server
                                            }, 1000);
                                        })
                                },1000)}
                            })} else {
                        $.getJSON('db/game.json', function (game) {
                            let game_length = game.length;
                            if (game_length === 9) {
                                let substring = "wint";
                                let string = name_lbl.innerText;
                                if (!string.includes(substring)) {
                                    setTimeout(function () {
                                        name_lbl.innerHTML = "";
                                        name_lbl.innerHTML = "Het is gelijk spel!";
                                        $.ajax({
                                            method: "POST", url: "win.php"
                                        })
                                            .done(function ()  {
                                                setTimeout(function () {
                                                    location.reload(true);
                                                }, 1000)
                                            })
                                    }, 1000)
                                }

                            }
                        })
                    }
                }
            }
        })
    }

    let clicked = [];
    $(".ttt_btn").on("click", function() {
        // Verandert de turn per click en slaat de plaats en het soort click op game.json
        // wanneer er op een button in het speelveld wordt geclickt
        // en roept de functies: getDatabase, aanZet en victory aan.
        let id = this.id;
        $.getJSON('db/turn.json', function (json) {
            let turn = json[0];
            console.log(turn);
            if ( clicked.includes(id) === false) {
                //let html = "<i class=\"fas fa-times\"></i>";
                let html = "<i class=" +markers[turn]+ "></i>";
                clicked.push(id);
                $.ajax(
                    {method: "POST",
                        url: "addclicks.php",
                        data: {id: id, html: html}
                    })
                    .done(function () {
                        getDatabase();
                        aanZet();
                        victory();

                    })
            }})
        });

    getDatabase();
    aanZet();
    victory();
    let vicint = setInterval(victory, 500);
    let penint = setInterval(aanZet, 100);
    setInterval(getDatabase,100);

});