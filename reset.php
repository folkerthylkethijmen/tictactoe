<?php
    // Leegt alle .json-files uit de database-folder, m.u.v. players.json
    $gamedata = file_get_contents('db/game.json');
    $game = json_decode($gamedata);
    foreach ($game as $i => $value) {
        unset($game[$i]);
    }
    $game_output = json_encode($game);
    file_put_contents("db/game.json", $game_output);

    $turndata = file_get_contents('db/turn.json');
    $turn = json_decode($turndata);
    $turn = array(0);
    $turn_output = json_encode($turn);
    file_put_contents("db/turn.json", $turn_output);

    $chatdata = file_get_contents('db/chat.json');
    $chat = json_decode($data);
    foreach ($chat as $i => $value) {
        unset($chat[$i]);
    }
    $chat_output = json_encode($chat);
    file_put_contents("db/chat.json", $chat_output);

?>