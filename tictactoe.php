<?PHP
if (isset($_GET['ajax'])) {
    $data = file_get_contents("db/players.json"); /* Leest books.json in als string */
    header('Content-Type: application/json'); /* Stel de header in */
    echo $data;
    die();
}
?>

<!doctype html>
<!-- Dit bestand betreft de game-pagina van TicTacToe -->
<html lang="nl">

<head>
    <title>Tic Tac Toe</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/tictactoe.css">
    <link href="https://fonts.googleapis.com/css?family=Bowlby+One+SC" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>

<body>

    <div class="title_section">
        <h1>Tic Tac Toe</h1>
    </div>

    <div class="main_container">

        <div class="players">

            <div class="player_boxes">

                <div class="player1">
                    <div class="name1">
                    </div>

                </div>

                <div class="player2">
                    <div class="name2">
                    </div>
                </div>

            </div>

        </div>

        <div class="aanzet">

            <span id="name"></span>
        </div>

        <div class="tictactoe">
            <button id="one" class="ttt_btn"></button>
            <button id="two" class="ttt_btn"></button>
            <button id="three" class="ttt_btn"></button>
            <button id="four" class="ttt_btn"></button>
            <button id="five" class="ttt_btn"></button>
            <button id="six" class="ttt_btn"></button>
            <button id="seven" class="ttt_btn"></button>
            <button id="eigth" class="ttt_btn"></button>
            <button id="nine" class="ttt_btn"></button>
        </div>

        <div class="chatbox_container">
            <div class="chatbox" id="chatbox">
            </div>
            <form id="chatform">
                <input class="msg_send" id="msg_send">
                <input id="send_btn" class="send_btn" type="submit" value="Verstuur">
            </form>
        </div>

    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/chat.js"></script>
<script src="js/tictactoe.js"></script>
</body>
</html>