<?php
    // Reset game.json & zet turn.json op 0
    $data = file_get_contents('db/game.json');
    $game = json_decode($data);
    foreach ($game as $i => $value) {
        unset($game[$i]);
    }
    $output = json_encode($game);
    file_put_contents("db/game.json", $output);

    $turndata = file_get_contents('db/turn.json');
    $turn = json_decode($turndata);
    $turn = array(0);
    $turn_output = json_encode($turn);
    file_put_contents("db/turn.json", $turn_output);

?>